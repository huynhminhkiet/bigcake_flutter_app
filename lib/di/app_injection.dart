import 'package:bigcake_flutter_app/data/local/prefs/app_shared_prefs.dart';
import 'package:bigcake_flutter_app/di/module/network_module.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';

class AppInjection {
  static init() {
    final injector = Injector.getInjector("app_injection");
    NetworkModule.map(injector);
    injector.map<AppSharedPreferences>((i) => new AppSharedPreferences(), isSingleton: true);
  }

  static Injector injector() {
    return Injector.getInjector("app_injection");
  }
}