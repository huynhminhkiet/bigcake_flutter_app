import 'package:flutter_simple_dependency_injection/injector.dart';

class StateInjection {
  static init() {
    final stateInjection = Injector.getInjector("state_injector");
  }

  static Injector injector() {
    return Injector.getInjector("state_injector");
  }

  static dispose() {
    Injector.getInjector("state_injector").dispose();
  }
}