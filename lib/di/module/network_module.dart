import 'package:bigcake_flutter_app/data/remote/remote_source.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:dio/dio.dart';


class NetworkModule {
  static map(Injector injector) {
    //dio
    injector.map<Dio>((i) {

    }, isSingleton: true);

    // remote source
    injector.map<RemoteSource>((i) {
      return new RemoteSource(_getDio());
    }, isSingleton: true);
  }

  static Dio _getDio() {
    Dio dio = new Dio();

    dio.options.baseUrl = "https://jsonplaceholder.typicode.com";

    dio.interceptor.request.onSend = (Options options) {
      print("${options.method} ${options.baseUrl}${options.path}");
      print("headers: ${options.headers}");
      return options; //continue
    };
    dio.interceptor.response.onSuccess = (Response response) {
      print("status: ${response.statusCode}");
      print(response.toString());
      return response; // continue
    };
    dio.interceptor.response.onError = (DioError e){
      print("${e.message} \n ${e.response}");
      return  e;//continue
    };

    return dio;
  }
}