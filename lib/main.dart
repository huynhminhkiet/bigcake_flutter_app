import 'package:bigcake_flutter_app/di/app_injection.dart';
import 'package:bigcake_flutter_app/ui/main/main_page.dart';
import 'package:flutter/material.dart';

void main() {
  // Injection
  AppInjection.init();

  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}
