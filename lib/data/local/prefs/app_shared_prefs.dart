import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

class AppSharedPreferences {
  void setAppHasOpenInFirstTime() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(APP_HAS_OPEN_IN_FIRST_TIME, true);
  }

  Future<bool> isAppHasOpenInFirstTime() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(APP_HAS_OPEN_IN_FIRST_TIME);
  }
}

const APP_HAS_OPEN_IN_FIRST_TIME = "APP_HAS_OPEN_IN_FIRST_TIME";
