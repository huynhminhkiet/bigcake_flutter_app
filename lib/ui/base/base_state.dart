import 'package:bigcake_flutter_app/di/state_injection.dart';
import 'package:flutter/material.dart';

abstract class BaseState<T extends StatefulWidget> extends State<T> {

  @override
  void initState() {
    super.initState();
    StateInjection.init();
    onInitState();
  }

  @override
  Widget build(BuildContext context) {
    return onBuild(context);
  }

  @override
  void dispose() {
    onDispose();
    StateInjection.dispose();
    super.dispose();
  }

  void onInitState();
  Widget onBuild(BuildContext context);
  void onDispose();
}