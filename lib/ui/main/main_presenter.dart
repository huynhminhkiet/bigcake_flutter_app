import 'package:bigcake_flutter_app/data/local/prefs/app_shared_prefs.dart';
import 'package:bigcake_flutter_app/data/remote/remote_source.dart';
import 'package:bigcake_flutter_app/di/app_injection.dart';
import 'package:bigcake_flutter_app/ui/base/base_presenter.dart';
import 'package:bigcake_flutter_app/ui/base/mpv_view.dart';
import 'package:bigcake_flutter_app/ui/main/main_view.dart';

class MainPresenter extends BasePresenter<MainView> {
  AppSharedPreferences _sharedPreferences;
  RemoteSource _remoteSource;

  MainPresenter() {
    _sharedPreferences = AppInjection.injector().get<AppSharedPreferences>();
    _remoteSource = AppInjection.injector().get<RemoteSource>();
  }

  @override
  void onAttachView(MvpView view) async {
    super.onAttachView(view);
    bool isAppHasOpenInFirstTime = await _sharedPreferences.isAppHasOpenInFirstTime();
    if (isAppHasOpenInFirstTime == null)
      _sharedPreferences.setAppHasOpenInFirstTime();
    getMvpView?.showText(isAppHasOpenInFirstTime.toString());
    _remoteSource.getTodo();
  }
}
