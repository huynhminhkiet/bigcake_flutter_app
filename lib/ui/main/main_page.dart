import 'package:bigcake_flutter_app/ui/base/base_state.dart';
import 'package:bigcake_flutter_app/ui/main/main_presenter.dart';
import 'package:bigcake_flutter_app/ui/main/main_view.dart';
import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends BaseState<MyHomePage> implements MainView {
  var _presenter;
  String text;

  @override
  void onInitState() {
    _presenter = new MainPresenter();
    _presenter.onAttachView(this);
  }

  @override
  Widget onBuild(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(widget.title),
        ),
        body: new Center(
          child: text != null ? Text(text) : null,
        )
    );
  }

  @override
  void onDispose() {
    _presenter.onDetachView();
  }

  @override
  showText(String text) {
    setState(() {
      this.text = text;
    });
  }


}