import 'package:bigcake_flutter_app/ui/base/mpv_view.dart';

abstract class MainView extends MvpView {
  showText(String text);
}